$(document).ready(function () {
  $("#problemTwoButton").click(function (event) {
    event.preventDefault();
    let query = $("#query").value;
    let url = `https://api.github.com/search/repositories?q={{${query}}}`;
    $.ajax({
      url: url,
      method: "GET",
      dataType: "json",
      success: async function (result) {
        let output = await result.items.reduce((object, array) => {
          $.ajax({
            url: array.owner.url,
            method: "GET",
            dataType: "json",
            success: function (result) {
              const owner = {
                name: result.name,
                login: array.owner.login,
                followerCount: result.followers,
                followingCount: result.following,
              };
              array.owner = owner;
              let branch_url = array.branches_url.split("{");
              let b_url = branch_url[0];
              $.ajax({
                url: b_url,
                method: "GET",
                dataType: "json",
                success: function (result) {
                  let numberOfBranch = result.length;
                  const subset = {
                    name: array.name,
                    fullname: array.full_name,
                    private: array.private,
                    owner: array.owner,
                    licenseName: array.license ? array.license.name : " ",
                    score: array.score,
                    numberOfBranch: numberOfBranch,
                  };
                  console.log(subset);
                  return subset;
                },
              });
            },
          });
        });
      },
    });
  });
});
