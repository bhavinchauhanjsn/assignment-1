$(document).ready(function () {
  $("#problemOneButton").click(function () {
    $.ajax({
      url: "https://api.jsonbin.io/b/5f917576adfa7a7bbea6cccf",
      method: "GET",
      dataType: "json",
      success: function (result) {
        let output = result.reduce(
          function (total, currentValue) {
            total.attacker[currentValue.attacker_king] =
              (total.attacker[currentValue.attacker_king] || 0) + 1;

            total.defender[currentValue.defender_king] =
              (total.defender[currentValue.defender_king] || 0) + 1;

            total.region[currentValue.region] =
              (total.region[currentValue.region] || 0) + 1;

            total.name[currentValue.name] =
              (total.name[currentValue.name] || 0) + 1;

            if (currentValue.attacker_outcome != "") {
              total.attacker_outcome[currentValue.attacker_outcome] =
                (total.attacker_outcome[currentValue.attacker_outcome] || 0) +
                1;
            }

            if (currentValue.battle_type != "") {
              total.battles[currentValue.battle_type] =
                (total.battles[currentValue.battle_type] || 0) + 1;
            }

            total.max = Math.max(total.max, currentValue.defender_size);
            total.min = Math.min(total.min, currentValue.defender_size);
            total.sum += currentValue.defender_size;

            return total;
          },
          {
            attacker: {},
            defender: {},
            region: {},
            name: {},
            attacker_outcome: {},
            battles: {},
            min: Number.MIN_VALUE,
            max: Number.MAX_VALUE,
            sum: 0,
          }
        );

        function calculateMax(name) {
          let maxName = Object.keys(output.name)[
            Object.values(output.name).indexOf(
              Math.max(...Object.values(output.name))
            )
          ];
          return maxName;
        }

        most_active = {
          attacker_king: calculateMax("attacker"),
          defender_king: calculateMax("defender"),
          region: calculateMax("region"),
          name: calculateMax("name"),
        };

        defender_size = {
          average: output.sum / result.length,
          min: output.min,
          max: output.max,
        };

        let attacker_outcome = output.attacker_outcome;
        let battle_type = Object.keys(output.battles);

        let final = {
          most_active,
          attacker_outcome,
          battle_type,
          defender_size,
        };

        $("#problemOneDiv").append(
          "{<br>'most_active':{<br>'attacker_king': " +
            JSON.stringify(final.most_active.attacker_king) +
            ",<br>'defender_king': " +
            JSON.stringify(final.most_active.defender_king) +
            ",<br>'region': " +
            JSON.stringify(final.most_active.region) +
            ",<br>'name': " +
            JSON.stringify(final.most_active.name) +
            ",<br>},<br>'attacker_outcome':{<br>'win': " +
            JSON.stringify(final.attacker_outcome.win) +
            ",<br>'loss': " +
            JSON.stringify(final.attacker_outcome.loss) +
            "<br>},<br>'battle_type': " +
            JSON.stringify(final.battle_type) +
            ",<br>'defender_size':{<br>'average': " +
            JSON.stringify(final.defender_size.average) +
            ",<br>'min': " +
            JSON.stringify(final.defender_size.min) +
            ",<br>'max': " +
            JSON.stringify(final.defender_size.max) +
            "<br>}<br>}"
        );

        console.log(JSON.stringify(final, null, 2));
      },
    });
  });
});
